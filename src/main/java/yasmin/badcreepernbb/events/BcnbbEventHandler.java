package yasmin.badcreepernbb.events;

import java.util.Iterator;
import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.monster.CreeperEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.world.ExplosionEvent.Detonate;
import net.minecraftforge.event.entity.EntityMobGriefingEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.eventbus.api.Event.Result;
import yasmin.badcreepernbb.capabilities.UserSettingsStorageProvider;
import yasmin.badcreepernbb.capabilities.UserSettings;
import yasmin.badcreepernbb.capabilities.IUserSettings;
import yasmin.badcreepernbb.config.Settings;
import yasmin.badcreepernbb.entities.BcnbbRegion;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class BcnbbEventHandler {
	public static final ResourceLocation SETTINGS_CAP = new ResourceLocation("badcreepernbb", "bcnbbsettings");

	@SubscribeEvent
	public void mobGriefing(EntityMobGriefingEvent event) {
		if(event.getEntity() instanceof CreeperEntity) {
			BlockPos pos = event.getEntity().getPosition();
			
			if(Settings.rangeActive) {
				int r = Settings.range;
				BlockPos pos1 = pos.add(r, r, r);
				BlockPos pos2 = pos.add(-r, -r, -r);
				
				AxisAlignedBB bb = new AxisAlignedBB(pos1, pos2);
				List<PlayerEntity> players = event.getEntity().getEntityWorld().getEntitiesWithinAABB(PlayerEntity.class, bb);
				
				for(PlayerEntity player : players) {
					IUserSettings cap = player.getCapability(UserSettingsStorageProvider.SETTINGS_CAP).orElse(new UserSettings());
					
					if(cap.getIsActive()) {
						event.setResult(Result.DENY);
						break;
					}
				}
			}
			
			if (Settings.regionActive) {
				for(BcnbbRegion region : Settings.protectedRegions) {
					AxisAlignedBB bb = new AxisAlignedBB(region.blockPos1, region.blockPos2);
					
					if(bb.contains(event.getEntity().getPositionVec())) {
						event.setResult(Result.DENY);
						break;
					}
				}
			}
		}
	}
	
	@SubscribeEvent
	public void onExplosionDetonate(Detonate event) {
		if(event.getExplosion().getExplosivePlacedBy() instanceof CreeperEntity) {
			BlockPos pos = event.getExplosion().getExplosivePlacedBy().getPosition();
			
			if(Settings.rangeActive) {
				int r = Settings.range;
				BlockPos pos1 = pos.add(r, r, r);
				BlockPos pos2 = pos.add(-r, -r, -r);
				
				AxisAlignedBB bb = new AxisAlignedBB(pos1, pos2);
				List<PlayerEntity> players = event.getWorld().getEntitiesWithinAABB(PlayerEntity.class, bb);
				
				for(PlayerEntity player : players) {
					IUserSettings cap = player.getCapability(UserSettingsStorageProvider.SETTINGS_CAP).orElse(new UserSettings());
					
					if(cap.getIsActive()) {
						Iterator<Entity> iter = event.getAffectedEntities().iterator();
						
						while(iter.hasNext()) {
							Entity entity = iter.next();
							
							if(entity instanceof ItemEntity) {
								iter.remove();
							}
						}
						
						break;
					}
				}
			}
			
			if (Settings.regionActive) {
				for(BcnbbRegion region : Settings.protectedRegions) {
					AxisAlignedBB bb = new AxisAlignedBB(region.blockPos1, region.blockPos2);
					
					if(bb.contains(event.getExplosion().getExplosivePlacedBy().getPositionVec())) {
						Iterator<Entity> iter = event.getAffectedEntities().iterator();
						
						while(iter.hasNext()) {
							Entity entity = iter.next();
							
							if(entity instanceof ItemEntity) {
								iter.remove();
							}
						}
						
						break;
					}
				}
			}
		}
	}
	
	@SubscribeEvent
	public void attachCapability(AttachCapabilitiesEvent<Entity> event) {
		if(event.getObject() instanceof PlayerEntity) {
			event.addCapability(SETTINGS_CAP, new UserSettingsStorageProvider());
		}
	}
	
	@SubscribeEvent
	public void onPlayerLogin(PlayerLoggedInEvent event) {
		PlayerEntity player = event.getPlayer();
		IUserSettings cap = player.getCapability(UserSettingsStorageProvider.SETTINGS_CAP).orElse(new UserSettings());
		
		String message = "Your setting for BCNBB is " + cap.getIsActive();
		player.sendMessage(new StringTextComponent(message));
	}
	
	@SubscribeEvent
	public void onPlayerClone(PlayerEvent.Clone event) {
		PlayerEntity player = event.getPlayer();
		IUserSettings cap = player.getCapability(UserSettingsStorageProvider.SETTINGS_CAP).orElse(new UserSettings());
		IUserSettings old = event.getOriginal().getCapability(UserSettingsStorageProvider.SETTINGS_CAP).orElse(new UserSettings());
		
		cap.setIsActive(old.getIsActive());
	}
}