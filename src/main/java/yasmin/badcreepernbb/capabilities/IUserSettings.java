package yasmin.badcreepernbb.capabilities;

import net.minecraft.util.math.BlockPos;

public interface IUserSettings {
	public void setIsActive(boolean isActive);
	public boolean getIsActive();
	public void setRegionPos1(BlockPos pos);
	public BlockPos getRegionPos1();
	public void setRegionPos2(BlockPos pos);
	public BlockPos getRegionPos2();
}