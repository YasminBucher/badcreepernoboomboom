package yasmin.badcreepernbb.capabilities;

import net.minecraft.util.math.BlockPos;

public class UserSettings implements IUserSettings {
	private boolean active = false;
	private BlockPos pos1 = null;
	private BlockPos pos2 = null;
	
	@Override
	public void setIsActive(boolean isActive) {
		this.active = isActive;
	}

	@Override
	public boolean getIsActive() {
		return this.active;
	}

	@Override
	public void setRegionPos1(BlockPos pos) {
		this.pos1 = pos;
	}

	@Override
	public BlockPos getRegionPos1() {
		return this.pos1;
	}

	@Override
	public void setRegionPos2(BlockPos pos) {
		this.pos2 = pos;
	}

	@Override
	public BlockPos getRegionPos2() {
		return this.pos2;
	}
}