package yasmin.badcreepernbb.capabilities;

import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraft.nbt.INBT;

public class UserSettingsStorageProvider implements ICapabilitySerializable<INBT> {
	@CapabilityInject(value = IUserSettings.class)
	public static final Capability<IUserSettings> SETTINGS_CAP = null;
	
	private IUserSettings instance = SETTINGS_CAP.getDefaultInstance();
	private final LazyOptional<IUserSettings> holder = LazyOptional.of(() -> this.instance);
	
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
		return SETTINGS_CAP.orEmpty(cap, holder);
	}

	@Override
	public INBT serializeNBT() {
		return SETTINGS_CAP.getStorage().writeNBT(SETTINGS_CAP, this.instance, null);
	}

	@Override
	public void deserializeNBT(INBT nbt) {
		SETTINGS_CAP.getStorage().readNBT(SETTINGS_CAP, this.instance, null, nbt);
	}
}