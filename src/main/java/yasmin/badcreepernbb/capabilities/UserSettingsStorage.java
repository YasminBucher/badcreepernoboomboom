package yasmin.badcreepernbb.capabilities;

import net.minecraft.nbt.ByteNBT;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;

public class UserSettingsStorage implements IStorage<IUserSettings> {
	@Override
	public INBT writeNBT(Capability<IUserSettings> capability, IUserSettings instance, Direction side) {
		BlockPos bp1 = BlockPos.ZERO;
		BlockPos bp2 = BlockPos.ZERO;
		
		if(instance.getRegionPos1() != null) {
			bp1 = instance.getRegionPos1();
		}
		
		if(instance.getRegionPos2() != null) {
			bp2 = instance.getRegionPos2();
		}
		
		CompoundNBT nbtBlockPos1 = new CompoundNBT();
		nbtBlockPos1.putInt("x", bp1.getX());
		nbtBlockPos1.putInt("y", bp1.getY());
		nbtBlockPos1.putInt("z", bp1.getZ());
		
		CompoundNBT nbtBlockPos2 = new CompoundNBT();
		nbtBlockPos2.putInt("x", bp2.getX());
		nbtBlockPos2.putInt("y", bp2.getY());
		nbtBlockPos2.putInt("z", bp2.getZ());
		
		CompoundNBT nbt = new CompoundNBT();
		nbt.putBoolean("isActive", instance.getIsActive());
		nbt.put("blockPos1", nbtBlockPos1);
		nbt.put("blockPos2", nbtBlockPos2);
		
		return nbt;
	}

	@Override
	public void readNBT(Capability<IUserSettings> capability, IUserSettings instance, Direction side, INBT nbt) {
		if(nbt instanceof ByteNBT) {
			instance.setIsActive(((ByteNBT) nbt).getByte() == 1);
			instance.setRegionPos1(BlockPos.ZERO);
			instance.setRegionPos2(BlockPos.ZERO);
		} else if(nbt instanceof CompoundNBT) {
			CompoundNBT cnbt = (CompoundNBT)nbt;
			CompoundNBT cnbtBlockPos1 = cnbt.getCompound("blockPos1");
			CompoundNBT cnbtBlockPos2 = cnbt.getCompound("blockPos2");
			
			instance.setIsActive(cnbt.getBoolean("isActive"));
			instance.setRegionPos1(new BlockPos(cnbtBlockPos1.getInt("x"), cnbtBlockPos1.getInt("y"), cnbtBlockPos1.getInt("z")));
			instance.setRegionPos2(new BlockPos(cnbtBlockPos2.getInt("x"), cnbtBlockPos2.getInt("y"), cnbtBlockPos2.getInt("z")));
		}
	}
}