package yasmin.badcreepernbb.config;

import java.util.ArrayList;
import java.util.List;

import yasmin.badcreepernbb.entities.BcnbbRegion;

public class Settings {
	public static int range = 5;
	public static boolean showDebugMessages = false;
	public static boolean rangeActive = true;
	public static boolean regionActive = false;
	public static List<BcnbbRegion> protectedRegions = new ArrayList<BcnbbRegion>();
}