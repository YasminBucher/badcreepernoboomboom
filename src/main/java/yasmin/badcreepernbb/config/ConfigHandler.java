package yasmin.badcreepernbb.config;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.ForgeConfigSpec.BooleanValue;
import net.minecraftforge.common.ForgeConfigSpec.Builder;
import net.minecraftforge.common.ForgeConfigSpec.IntValue;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.config.ModConfig.Loading;
import net.minecraftforge.fml.config.ModConfig.Reloading;
import net.minecraftforge.fml.config.ModConfig.Type;
import yasmin.badcreepernbb.BadCreeperNoBoomBoom;
import yasmin.badcreepernbb.entities.BcnbbRegion;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.lang3.tuple.Pair;

public class ConfigHandler {
	private static Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	
	public static final ForgeConfigSpec commonSpec;
	public static final ConfigCommon COMMON;
	
	static {
		final Pair<ConfigCommon, ForgeConfigSpec> commonSpecPair = new ForgeConfigSpec.Builder().configure(ConfigCommon::new);
		commonSpec = commonSpecPair.getRight();
		COMMON = commonSpecPair.getLeft();
	}
	
	public static class ConfigCommon{
		public final IntValue range;
		public final BooleanValue showDebugMessages;
		public final BooleanValue rangeActive;
		public final BooleanValue regionActive;
		
		private ConfigCommon(Builder builder) {
			builder.comment("Common Settings").push("common");
			
			range = getInt(builder, "Range", 5, 0, 100, "The range around the creeper explosion that is checked for players.");
			showDebugMessages = getBool(builder, "ShowDebugMessages", false, "Determines if debug messages will be shown.");
			rangeActive = getBool(builder, "RangeActive", true, "If the creeper explosion range check is active. If not, creepers will explode as usual.");
			regionActive = getBool(builder, "RegionActive", false, "If the creeper region protection is active.");
			
			builder.pop();
		}
		
		private void apply() {
			Settings.range = range.get();
			Settings.showDebugMessages = showDebugMessages.get();
			Settings.rangeActive = rangeActive.get();
			Settings.regionActive = regionActive.get();
		}
		
		public void save() {
			commonSpec.save();
		}
		
		public void saveRegions() {
			saveRegionsToFile();
		}
	}
	
	private static IntValue getInt(Builder builder, String var, int def, int min, int max, String com){
        builder.comment(com);
        builder.translation(BadCreeperNoBoomBoom.MODID + ".config." + var.replaceAll(" ", "_"));
        return builder.defineInRange(var, def, min, max);
    }
	
	private static BooleanValue getBool(Builder builder, String var, boolean def, String com) {
		builder.comment(com);
        builder.translation(BadCreeperNoBoomBoom.MODID + ".config." + var.replaceAll(" ", "_"));
		return builder.define(var, def);
	}
	
	private static void loadRegions() {
		File regionsFile = new File("config/bcnbb_regions.json");
		
		if (regionsFile.exists()) {
			try {
				InputStreamReader isr = new InputStreamReader(new FileInputStream(regionsFile), StandardCharsets.UTF_8);
				java.lang.reflect.Type typeList = new TypeToken<ArrayList<BcnbbRegion>>() {}.getType();
				List<BcnbbRegion> regions = GSON.fromJson(isr, typeList);
				isr.close();
				
				Settings.protectedRegions = regions;
			} catch (Exception e) {
				// TODO: handle exception
			}
		} else {
			saveRegionsToFile();
		}
	}
	
	private static void saveRegionsToFile() {
		File regionsFile = new File("config/bcnbb_regions.json");
		
		try {
			OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(regionsFile), StandardCharsets.UTF_8);
			String json = GSON.toJson(Settings.protectedRegions);
			osw.write(json);
			osw.flush();
			osw.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@SubscribeEvent
	public static void onLoad(final Loading event) {
		if(BadCreeperNoBoomBoom.MODID.equalsIgnoreCase(event.getConfig().getModId())) {
			if(event.getConfig().getType() == Type.COMMON) {
				COMMON.apply();
				loadRegions();
			}
		}
	}
	 
	@SubscribeEvent
	public static void onFileChanged(Reloading event) {
		if(BadCreeperNoBoomBoom.MODID.equalsIgnoreCase(event.getConfig().getModId())) {
			if(event.getConfig().getType() == Type.COMMON) {
				COMMON.apply();
				loadRegions();
			}
		}
	}
}