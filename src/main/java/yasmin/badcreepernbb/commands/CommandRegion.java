package yasmin.badcreepernbb.commands;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.arguments.BlockPosArgument;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import yasmin.badcreepernbb.capabilities.UserSettingsStorageProvider;
import yasmin.badcreepernbb.capabilities.UserSettings;
import yasmin.badcreepernbb.capabilities.IUserSettings;
import yasmin.badcreepernbb.config.ConfigHandler;
import yasmin.badcreepernbb.config.Settings;
import yasmin.badcreepernbb.entities.BcnbbRegion;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;

public class CommandRegion {
	private static final int MAXREGIONSIZE = 10000;
	
	public static void register(CommandDispatcher<CommandSource> dispatch) {
		LiteralArgumentBuilder<CommandSource> argBuilder = Commands.literal("bcnbb_region")
				.requires(src -> src.hasPermissionLevel(0))
				.requires(src -> src.getEntity() instanceof ServerPlayerEntity)
				.executes(CommandRegion::executeRegion)
				.then(Commands.argument("block1", BlockPosArgument.blockPos())
						.then(Commands.argument("block2", BlockPosArgument.blockPos())
								.executes(CommandRegion::executeRegionWithBlockPos)))
				.then(Commands.literal("setpos1")
						.requires(src -> src.getEntity() instanceof ServerPlayerEntity)
						.executes(CommandRegion::executeSetPos1)
						.then(Commands.argument("blockPos", BlockPosArgument.blockPos())
								.executes(CommandRegion::executeSetPos1WithBlockPos)))
				.then(Commands.literal("setpos2")
						.requires(src -> src.getEntity() instanceof ServerPlayerEntity)
						.executes(CommandRegion::executeSetPos2)
						.then(Commands.argument("blockPos", BlockPosArgument.blockPos())
								.executes(CommandRegion::executeSetPos2WithBlockPos)))
				.then(Commands.argument("active", BoolArgumentType.bool())
						.requires(src -> src.hasPermissionLevel(4))
						.executes(CommandRegion::executeSetActive));
		
		dispatch.register(argBuilder);
	}
	
	private static int executeRegion(CommandContext<CommandSource> context) throws CommandSyntaxException {
		if (Settings.regionActive) {
			PlayerEntity player = context.getSource().asPlayer();
			IUserSettings cap = player.getCapability(UserSettingsStorageProvider.SETTINGS_CAP).orElse(new UserSettings());
			BlockPos bp1 = cap.getRegionPos1();
			BlockPos bp2 = cap.getRegionPos2();
			setRegion(context.getSource(), bp1, bp2);
		} else {
			notifyDisabled(context.getSource());
		}
		
		return 1;
	}
	
	private static int executeRegionWithBlockPos(CommandContext<CommandSource> context) throws CommandSyntaxException {
		if (Settings.regionActive) {
			BlockPos bp1 = context.getArgument("block1", BlockPos.class);
			BlockPos bp2 = context.getArgument("block2", BlockPos.class);
			setRegion(context.getSource(), bp1, bp2);
		} else {
			notifyDisabled(context.getSource());
		}
		
		return 1;
	}
	
	private static void setRegion(CommandSource src, BlockPos bp1, BlockPos bp2) throws CommandSyntaxException {
		if (Settings.showDebugMessages) {
			if (bp1 == null) {
				src.sendFeedback(new StringTextComponent("bp1 is null."), true);
			} else {
				src.sendFeedback(new StringTextComponent("bp1 is " + bp1.toString()), true);
			}
			
			if (bp2 == null) {
				src.sendFeedback(new StringTextComponent("bp2!2 is null."), true);
			} else {
				src.sendFeedback(new StringTextComponent("bp2 is " + bp2.toString()), true);
			}
		}
		
		if (bp1 != null && bp2 != null && !bp1.equals(BlockPos.ZERO) && !bp2.equals(BlockPos.ZERO)) {
			AxisAlignedBB aabb = new AxisAlignedBB(bp1, bp2);
			double size = aabb.getXSize() * aabb.getYSize() * aabb.getZSize();
			
			if (Settings.showDebugMessages) {
				src.sendFeedback(new StringTextComponent("BCNBB region size is " + size), true);
			}
			
			if (size > MAXREGIONSIZE) {
				StringTextComponent stc = new StringTextComponent("BCNBB region is too big (max size = " + MAXREGIONSIZE + ").");
				
				if (src.getEntity() instanceof PlayerEntity) {
					src.asPlayer().sendMessage(stc);
				} else {
					src.sendFeedback(stc, true);
				}
			} else {
				if (src.getEntity() instanceof PlayerEntity) {
					String uuid = src.asPlayer().getCachedUniqueIdString();
					
					if (Settings.showDebugMessages) {
						src.sendFeedback(new StringTextComponent("Player UUID is " + uuid), true);
					}
					
					if (uuid == null || StringUtils.isBlank(uuid)) {
						src.sendFeedback(new StringTextComponent("Player UUID is null or empty/whitespace."), true);
					} else {
						if (Settings.showDebugMessages) {
							src.sendFeedback(new StringTextComponent("Trying to get already existing reagion for user."), true);
							src.sendFeedback(new StringTextComponent("Protected regions are " + Settings.protectedRegions.toString()), true);
						}
						
						BcnbbRegion savedRegion = null;
						
						try {
							savedRegion = getRegionForUser(uuid);
						} catch (Exception e) {
							if (Settings.showDebugMessages) {
								src.sendFeedback(new StringTextComponent("Error getting saved region " + e.getMessage()), true);
							}
						}
						
						if (savedRegion != null) {
							if (Settings.showDebugMessages) {
								src.sendFeedback(new StringTextComponent("Found a saved region. Updated."), true);
							}
							
							savedRegion.blockPos1 = bp1;
							savedRegion.blockPos2 = bp2;
						} else {
							if (Settings.showDebugMessages) {
								src.sendFeedback(new StringTextComponent("This is a new region. Added."), true);
							}
							
							Settings.protectedRegions.add(new BcnbbRegion(uuid, bp1, bp2));
						}
						
						ConfigHandler.COMMON.saveRegions();
					}
				} else {
					src.sendFeedback(new StringTextComponent("Only players may define regions."), true);
				}
			}
		} else {
			StringTextComponent stc = new StringTextComponent("BCNBB region is invalid.");
			
			if (src.getEntity() instanceof PlayerEntity) {
				src.asPlayer().sendMessage(stc);
			} else {
				src.sendFeedback(stc, true);
			}
		}
	}
	
	private static BcnbbRegion getRegionForUser(String uuid) throws Exception {
		try {
			List<BcnbbRegion> regions = Settings.protectedRegions;
			
			for(BcnbbRegion region : regions) {
				if (region.userUUID.equalsIgnoreCase(uuid)) {
					return region;
				}
			}
		} catch (Exception e) {
			throw e;
		}
		
		return null;
	}
	
	private static int executeSetPos1(CommandContext<CommandSource> context) throws CommandSyntaxException {
		return executeSetPos(context, 1, BlockPos.ZERO);
	}
	
	private static int executeSetPos2(CommandContext<CommandSource> context) throws CommandSyntaxException {
		return executeSetPos(context, 2, BlockPos.ZERO);
	}
	
	private static int executeSetPos1WithBlockPos(CommandContext<CommandSource> context) throws CommandSyntaxException {
		BlockPos bp = context.getArgument("blockPos", BlockPos.class);
		return executeSetPos(context, 1, bp);
	}
	
	private static int executeSetPos2WithBlockPos(CommandContext<CommandSource> context) throws CommandSyntaxException {
		BlockPos bp = context.getArgument("blockPos", BlockPos.class);
		return executeSetPos(context, 2, bp);
	}
	
	private static int executeSetPos(CommandContext<CommandSource> context, int posId, BlockPos bp) throws CommandSyntaxException {
		if (Settings.regionActive) {
			PlayerEntity player = context.getSource().asPlayer();
			IUserSettings cap = player.getCapability(UserSettingsStorageProvider.SETTINGS_CAP).orElse(new UserSettings());
			
			if (bp == null || bp.equals(BlockPos.ZERO)) {
				bp = player.getPosition();
			}
			
			if (posId == 1) {
				cap.setRegionPos1(bp);
			} else {
				cap.setRegionPos2(bp);
			}
			
			context.getSource().sendFeedback(new StringTextComponent("pos" + posId + " has been set to " + bp.toString()), true);
		} else {
			notifyDisabled(context.getSource());
		}
		
		return 1;
	}
	
	private static void notifyDisabled(CommandSource src) throws CommandSyntaxException {
		StringTextComponent stc = new StringTextComponent("BCNBB region protection is deactivated.");
		
		if (src.getEntity() instanceof PlayerEntity) {
			src.asPlayer().sendMessage(stc);
		} else {
			src.sendFeedback(stc, true);
		}
	}
	
	private static int executeSetActive(CommandContext<CommandSource> context) throws CommandSyntaxException {
		boolean active = context.getArgument("active", Boolean.class);
		
		ConfigHandler.COMMON.regionActive.set(active);
		ConfigHandler.COMMON.save();
		
		context.getSource().sendFeedback(new StringTextComponent("BCNBB region set to " + active), true);
		
		return 1;
	}
}