package yasmin.badcreepernbb.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import yasmin.badcreepernbb.config.ConfigHandler;

public class CommandDebug {
	public static void register(CommandDispatcher<CommandSource> dispatch) {
		LiteralArgumentBuilder<CommandSource> argBuilder = Commands.literal("bcnbb_debug")
				.requires(src -> src.hasPermissionLevel(0))
				.then(Commands.argument("showDebugMessages", BoolArgumentType.bool())
						.executes(CommandDebug::execute));
		
		dispatch.register(argBuilder);
	}
	
	private static int execute(CommandContext<CommandSource> context) throws CommandSyntaxException {
		boolean showDebugMessages = context.getArgument("showDebugMessages", Boolean.class);
		
		ConfigHandler.COMMON.showDebugMessages.set(showDebugMessages);
		ConfigHandler.COMMON.save();
		
		return 1;
	}
}