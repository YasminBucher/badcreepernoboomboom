package yasmin.badcreepernbb.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.StringTextComponent;
import yasmin.badcreepernbb.capabilities.UserSettingsStorageProvider;
import yasmin.badcreepernbb.capabilities.UserSettings;
import yasmin.badcreepernbb.capabilities.IUserSettings;
import yasmin.badcreepernbb.config.ConfigHandler;
import yasmin.badcreepernbb.config.Settings;

public class CommandRange {
	public static void register(CommandDispatcher<CommandSource> dispatch) {
		LiteralArgumentBuilder<CommandSource> argBuilder = Commands.literal("bcnbb")
				.requires(src -> src.hasPermissionLevel(0))
				.requires(src -> src.getEntity() instanceof ServerPlayerEntity)
				.then(Commands.argument("active", BoolArgumentType.bool())
						.executes(CommandRange::execute));
		
		dispatch.register(argBuilder);
		
		LiteralArgumentBuilder<CommandSource> argBuilderRange = Commands.literal("bcnbb_range")
				.requires(src -> src.hasPermissionLevel(0))
				.executes(CommandRange::executeGetRange)
				.then(Commands.argument("range", IntegerArgumentType.integer(0, 100))
						.requires(src -> src.hasPermissionLevel(4))
						.executes(CommandRange::executeRange))
				.then(Commands.argument("active", BoolArgumentType.bool())
						.requires(src -> src.hasPermissionLevel(4))
						.executes(CommandRange::executeSetActive));
		
		dispatch.register(argBuilderRange);
	}
	
	private static int execute(CommandContext<CommandSource> context) throws CommandSyntaxException {
		PlayerEntity player = context.getSource().asPlayer();
		
		if (Settings.rangeActive) {
			IUserSettings cap = player.getCapability(UserSettingsStorageProvider.SETTINGS_CAP).orElse(new UserSettings());
			
			boolean isActive = context.getArgument("active", Boolean.class);
			
			cap.setIsActive(isActive);
			
			player.sendMessage(new StringTextComponent("BCNBB set to " + isActive));
		} else {
			player.sendMessage(new StringTextComponent("BCNBB range protection is deactivated."));
		}
		
		return 1;
	}
	
	private static int executeRange(CommandContext<CommandSource> context) throws CommandSyntaxException{
		int range = context.getArgument("range", Integer.class);
		
		if(range != Settings.range) {
			ConfigHandler.COMMON.range.set(range);
			ConfigHandler.COMMON.save();
			
			if(context.getSource().getEntity() instanceof ServerPlayerEntity)
				context.getSource().asPlayer().sendMessage(new StringTextComponent("BCNBB range set to " + range));
			else
				context.getSource().sendFeedback(new StringTextComponent("BCNBB range set to " + range), true);
		}
		
		return 1;
	}
	
	private static int executeGetRange(CommandContext<CommandSource> context) throws CommandSyntaxException{
		int range = Settings.range;
		
		if(context.getSource().getEntity() instanceof ServerPlayerEntity)
			context.getSource().asPlayer().sendMessage(new StringTextComponent("Current BCNBB range is " + range));
		else
			context.getSource().sendFeedback(new StringTextComponent("Current BCNBB range is " + range), true);
		
		return 1;
	}
	
	private static int executeSetActive(CommandContext<CommandSource> context) throws CommandSyntaxException{
		boolean active = context.getArgument("active", Boolean.class);
		
		ConfigHandler.COMMON.rangeActive.set(active);
		ConfigHandler.COMMON.save();
		
		return 1;
	}
}