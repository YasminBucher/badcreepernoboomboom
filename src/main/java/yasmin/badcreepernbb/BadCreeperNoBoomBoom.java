package yasmin.badcreepernbb;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig.Type;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import yasmin.badcreepernbb.capabilities.UserSettings;
import yasmin.badcreepernbb.capabilities.UserSettingsStorage;
import yasmin.badcreepernbb.capabilities.IUserSettings;
import yasmin.badcreepernbb.commands.CommandDebug;
import yasmin.badcreepernbb.commands.CommandRange;
import yasmin.badcreepernbb.commands.CommandRegion;
import yasmin.badcreepernbb.config.ConfigHandler;
import yasmin.badcreepernbb.events.BcnbbEventHandler;

@Mod(BadCreeperNoBoomBoom.MODID)
public class BadCreeperNoBoomBoom {
	public static final String MODID = "badcreepernbb";
	
    private BcnbbEventHandler forgeEventHandler;
    
    public BadCreeperNoBoomBoom() {
    	ModLoadingContext.get().registerConfig(Type.COMMON, ConfigHandler.commonSpec);
    	
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::onCommonSetup);
        FMLJavaModLoadingContext.get().getModEventBus().register(ConfigHandler.class);
        
        MinecraftForge.EVENT_BUS.register(this);
        
        this.forgeEventHandler = new BcnbbEventHandler();
        MinecraftForge.EVENT_BUS.register(this.forgeEventHandler);
    }

    private void onCommonSetup(final FMLCommonSetupEvent event) {
    	CapabilityManager.INSTANCE.register(IUserSettings.class, new UserSettingsStorage(), () -> new UserSettings());
    	MinecraftForge.EVENT_BUS.addListener(this::onServerStart);
    }
    
    private void onServerStart(FMLServerStartingEvent event) {
    	CommandRange.register(event.getCommandDispatcher());
    	CommandRegion.register(event.getCommandDispatcher());
    	CommandDebug.register(event.getCommandDispatcher());
    }
}