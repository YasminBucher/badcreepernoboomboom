package yasmin.badcreepernbb.entities;

import net.minecraft.util.math.BlockPos;

public class BcnbbRegion {
	public String userUUID;
	public BlockPos blockPos1;
	public BlockPos blockPos2;
	
	public BcnbbRegion(String userUUID, BlockPos blockPos1, BlockPos blockPos2) {
		this.userUUID = userUUID;
		this.blockPos1 = blockPos1;
		this.blockPos2 = blockPos2;
	}
	
	@Override
	public String toString() {
		return "{" + this.userUUID + ", " + this.blockPos1.toString() + ", " + this.blockPos2.toString() + "}";
	}
}